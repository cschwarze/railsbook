module ApplicationHelper
  def render_stars(value)
    output="\u2606" * 5
    if (1..5).include? value 
      value.times do |i|
        output[i]="\u2605"
      end
    end
    output
  end
end
