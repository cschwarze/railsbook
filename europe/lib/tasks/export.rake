namespace :export do
  desc "Prints Country.all in seeds.rb way"
  task :seeds_format => :environment do
    bad_keys = ['created_at', 'id', 'updated_at']
    begin
      fileout = File.open("db/seeds.rb","a")
      Country.order(:id).all.each do |country|
      serialized = country.serializable_hash
                          .delete_if {|key,val| bad_keys.include? key}
      fileout.puts "Country.create(#{serialized})"
    end
    rescue Exception => e
      puts "Couldn't write to file db/seeds.rb"
      puts e.message
      puts e.backtrace.inspect
    ensure
      fileout.close
    end
  end
end