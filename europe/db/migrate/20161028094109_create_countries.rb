class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
      t.string :name
      t.integer :population, :limit => 8

      t.timestamps
    end
  end
end
